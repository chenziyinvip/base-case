Gurobi Optimizer version 9.0.1 build v9.0.1rc0 (mac64)
Optimize a model with 572 rows, 192 columns and 759 nonzeros
Model fingerprint: 0x07a240d8
Coefficient statistics:
  Matrix range     [2e-01, 1e+00]
  Objective range  [1e+00, 1e+00]
  Bounds range     [0e+00, 0e+00]
  RHS range        [1e+00, 9e+04]
Presolve removed 565 rows and 177 columns
Presolve time: 0.01s
Presolved: 7 rows, 15 columns, 21 nonzeros

Iteration    Objective       Primal Inf.    Dual Inf.      Time
       0    6.3699727e+05   5.500000e+00   0.000000e+00      0s
       4    6.3700127e+05   0.000000e+00   0.000000e+00      0s

Solved in 4 iterations and 0.02 seconds
Optimal objective  6.370012746e+05
TIME=: 0
battery_soc: 4
battery_discharging_power: <gurobi.Var x[1,0] (value 0.0)>
=======================================================
TIME=: 1
battery_soc: <gurobi.Var x[0,1] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,1] (value 0.0)>
=======================================================
TIME=: 2
battery_soc: <gurobi.Var x[0,2] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,2] (value 0.0)>
=======================================================
TIME=: 3
battery_soc: <gurobi.Var x[0,3] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,3] (value 0.0)>
=======================================================
TIME=: 4
battery_soc: <gurobi.Var x[0,4] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,4] (value 0.0)>
=======================================================
TIME=: 5
battery_soc: <gurobi.Var x[0,5] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,5] (value 0.0)>
=======================================================
TIME=: 6
battery_soc: <gurobi.Var x[0,6] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,6] (value 0.0)>
=======================================================
TIME=: 7
battery_soc: <gurobi.Var x[0,7] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,7] (value 0.0)>
=======================================================
TIME=: 8
battery_soc: <gurobi.Var x[0,8] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,8] (value 0.0)>
=======================================================
TIME=: 9
battery_soc: <gurobi.Var x[0,9] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,9] (value 0.0)>
=======================================================
TIME=: 10
battery_soc: <gurobi.Var x[0,10] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,10] (value 0.0)>
=======================================================
TIME=: 11
battery_soc: <gurobi.Var x[0,11] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,11] (value 0.0)>
=======================================================
TIME=: 12
battery_soc: <gurobi.Var x[0,12] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,12] (value 0.0)>
=======================================================
TIME=: 13
battery_soc: <gurobi.Var x[0,13] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,13] (value 0.0)>
=======================================================
TIME=: 14
battery_soc: <gurobi.Var x[0,14] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,14] (value 0.0)>
=======================================================
TIME=: 15
battery_soc: <gurobi.Var x[0,15] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,15] (value 0.0)>
=======================================================
TIME=: 16
battery_soc: <gurobi.Var x[0,16] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,16] (value 0.0)>
=======================================================
TIME=: 17
battery_soc: <gurobi.Var x[0,17] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,17] (value 0.0)>
=======================================================
TIME=: 18
battery_soc: <gurobi.Var x[0,18] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,18] (value 0.0)>
=======================================================
TIME=: 19
battery_soc: <gurobi.Var x[0,19] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,19] (value 0.0)>
=======================================================
TIME=: 20
battery_soc: <gurobi.Var x[0,20] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,20] (value 0.0)>
=======================================================
TIME=: 21
battery_soc: <gurobi.Var x[0,21] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,21] (value 0.0)>
=======================================================
TIME=: 22
battery_soc: <gurobi.Var x[0,22] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,22] (value 0.0)>
=======================================================
TIME=: 23
battery_soc: <gurobi.Var x[0,23] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,23] (value 0.0)>
=======================================================
TIME=: 24
battery_soc: <gurobi.Var x[0,24] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,24] (value 4.0)>
=======================================================
TIME=: 25
battery_soc: <gurobi.Var x[0,25] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,25] (value 0.0)>
=======================================================
TIME=: 26
battery_soc: <gurobi.Var x[0,26] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,26] (value 0.0)>
=======================================================
TIME=: 27
battery_soc: <gurobi.Var x[0,27] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,27] (value 0.0)>
=======================================================
TIME=: 28
battery_soc: <gurobi.Var x[0,28] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,28] (value 0.0)>
=======================================================
TIME=: 29
battery_soc: <gurobi.Var x[0,29] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,29] (value 0.0)>
=======================================================
TIME=: 30
battery_soc: <gurobi.Var x[0,30] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,30] (value 0.0)>
=======================================================
TIME=: 31
battery_soc: <gurobi.Var x[0,31] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,31] (value 0.0)>
=======================================================
TIME=: 32
battery_soc: <gurobi.Var x[0,32] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,32] (value 0.0)>
=======================================================
TIME=: 33
battery_soc: <gurobi.Var x[0,33] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,33] (value 4.0)>
=======================================================
TIME=: 34
battery_soc: <gurobi.Var x[0,34] (value 2.0)>
battery_discharging_power: <gurobi.Var x[1,34] (value 4.0)>
=======================================================
TIME=: 35
battery_soc: <gurobi.Var x[0,35] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,35] (value 0.0)>
=======================================================
TIME=: 36
battery_soc: <gurobi.Var x[0,36] (value 1.0)>
battery_charging_power: <gurobi.Var x[1,36] (value 4.0)>
=======================================================
TIME=: 37
battery_soc: <gurobi.Var x[0,37] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,37] (value 4.0)>
=======================================================
TIME=: 38
battery_soc: <gurobi.Var x[0,38] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,38] (value 4.0)>
=======================================================
TIME=: 39
battery_soc: <gurobi.Var x[0,39] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,39] (value 0.0)>
=======================================================
TIME=: 40
battery_soc: <gurobi.Var x[0,40] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,40] (value 0.0)>
=======================================================
TIME=: 41
battery_soc: <gurobi.Var x[0,41] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,41] (value 0.0)>
=======================================================
TIME=: 42
battery_soc: <gurobi.Var x[0,42] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,42] (value 0.0)>
=======================================================
TIME=: 43
battery_soc: <gurobi.Var x[0,43] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,43] (value 0.0)>
=======================================================
TIME=: 44
battery_soc: <gurobi.Var x[0,44] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,44] (value 0.0)>
=======================================================
TIME=: 45
battery_soc: <gurobi.Var x[0,45] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,45] (value 0.0)>
=======================================================
TIME=: 46
battery_soc: <gurobi.Var x[0,46] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,46] (value 0.0)>
=======================================================
TIME=: 47
battery_soc: <gurobi.Var x[0,47] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,47] (value 0.0)>
=======================================================
TIME=: 48
battery_soc: <gurobi.Var x[0,48] (value 2.0)>
battery_charging_power: <gurobi.Var x[1,48] (value 4.0)>
=======================================================
TIME=: 49
battery_soc: <gurobi.Var x[0,49] (value 3.0)>
battery_charging_power: <gurobi.Var x[1,49] (value 0.0)>
=======================================================
TIME=: 50
battery_soc: <gurobi.Var x[0,50] (value 3.0)>
battery_charging_power: <gurobi.Var x[1,50] (value 4.0)>
=======================================================
TIME=: 51
battery_soc: <gurobi.Var x[0,51] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,51] (value 4.0)>
=======================================================
TIME=: 52
battery_soc: <gurobi.Var x[0,52] (value 3.0)>
battery_charging_power: <gurobi.Var x[1,52] (value 4.0)>
=======================================================
TIME=: 53
battery_soc: <gurobi.Var x[0,53] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,53] (value 4.0)>
=======================================================
TIME=: 54
battery_soc: <gurobi.Var x[0,54] (value 3.0)>
battery_charging_power: <gurobi.Var x[1,54] (value 4.0)>
=======================================================
TIME=: 55
battery_soc: <gurobi.Var x[0,55] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,55] (value 0.0)>
=======================================================
TIME=: 56
battery_soc: <gurobi.Var x[0,56] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,56] (value 0.0)>
=======================================================
TIME=: 57
battery_soc: <gurobi.Var x[0,57] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,57] (value 0.0)>
=======================================================
TIME=: 58
battery_soc: <gurobi.Var x[0,58] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,58] (value 0.0)>
=======================================================
TIME=: 59
battery_soc: <gurobi.Var x[0,59] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,59] (value 0.0)>
=======================================================
TIME=: 60
battery_soc: <gurobi.Var x[0,60] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,60] (value 0.0)>
=======================================================
TIME=: 61
battery_soc: <gurobi.Var x[0,61] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,61] (value 0.0)>
=======================================================
TIME=: 62
battery_soc: <gurobi.Var x[0,62] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,62] (value 0.0)>
=======================================================
TIME=: 63
battery_soc: <gurobi.Var x[0,63] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,63] (value 0.0)>
=======================================================
TIME=: 64
battery_soc: <gurobi.Var x[0,64] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,64] (value 0.0)>
=======================================================
TIME=: 65
battery_soc: <gurobi.Var x[0,65] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,65] (value 0.0)>
=======================================================
TIME=: 66
battery_soc: <gurobi.Var x[0,66] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,66] (value 0.0)>
=======================================================
TIME=: 67
battery_soc: <gurobi.Var x[0,67] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,67] (value 0.0)>
=======================================================
TIME=: 68
battery_soc: <gurobi.Var x[0,68] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,68] (value 0.0)>
=======================================================
TIME=: 69
battery_soc: <gurobi.Var x[0,69] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,69] (value 0.0)>
=======================================================
TIME=: 70
battery_soc: <gurobi.Var x[0,70] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,70] (value 0.0)>
=======================================================
TIME=: 71
battery_soc: <gurobi.Var x[0,71] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,71] (value 0.0)>
=======================================================
TIME=: 72
battery_soc: <gurobi.Var x[0,72] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,72] (value 0.0)>
=======================================================
TIME=: 73
battery_soc: <gurobi.Var x[0,73] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,73] (value 0.0)>
=======================================================
TIME=: 74
battery_soc: <gurobi.Var x[0,74] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,74] (value 0.0)>
=======================================================
TIME=: 75
battery_soc: <gurobi.Var x[0,75] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,75] (value 0.0)>
=======================================================
TIME=: 76
battery_soc: <gurobi.Var x[0,76] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,76] (value 0.0)>
=======================================================
TIME=: 77
battery_soc: <gurobi.Var x[0,77] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,77] (value 0.0)>
=======================================================
TIME=: 78
battery_soc: <gurobi.Var x[0,78] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,78] (value 0.0)>
=======================================================
TIME=: 79
battery_soc: <gurobi.Var x[0,79] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,79] (value 0.0)>
=======================================================
TIME=: 80
battery_soc: <gurobi.Var x[0,80] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,80] (value 0.0)>
=======================================================
TIME=: 81
battery_soc: <gurobi.Var x[0,81] (value 4.0)>
battery_discharging_power: <gurobi.Var x[1,81] (value 4.0)>
=======================================================
TIME=: 82
battery_soc: <gurobi.Var x[0,82] (value 3.0)>
battery_discharging_power: <gurobi.Var x[1,82] (value 4.0)>
=======================================================
TIME=: 83
battery_soc: <gurobi.Var x[0,83] (value 2.0)>
battery_discharging_power: <gurobi.Var x[1,83] (value 4.0)>
=======================================================
TIME=: 84
battery_soc: <gurobi.Var x[0,84] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,84] (value 0.0)>
=======================================================
TIME=: 85
battery_soc: <gurobi.Var x[0,85] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,85] (value 0.0)>
=======================================================
TIME=: 86
battery_soc: <gurobi.Var x[0,86] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,86] (value 0.0)>
=======================================================
TIME=: 87
battery_soc: <gurobi.Var x[0,87] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,87] (value 0.0)>
=======================================================
TIME=: 88
battery_soc: <gurobi.Var x[0,88] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,88] (value 0.0)>
=======================================================
TIME=: 89
battery_soc: <gurobi.Var x[0,89] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,89] (value 0.0)>
=======================================================
TIME=: 90
battery_soc: <gurobi.Var x[0,90] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,90] (value 0.0)>
=======================================================
TIME=: 91
battery_soc: <gurobi.Var x[0,91] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,91] (value 0.0)>
=======================================================
TIME=: 92
battery_soc: <gurobi.Var x[0,92] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,92] (value 0.0)>
=======================================================
TIME=: 93
battery_soc: <gurobi.Var x[0,93] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,93] (value 0.0)>
=======================================================
TIME=: 94
battery_soc: <gurobi.Var x[0,94] (value 1.0)>
battery_discharging_power: <gurobi.Var x[1,94] (value 0.0)>
=======================================================
TIME=: 95
battery_soc: <gurobi.Var x[0,95] (value 1.0)>
=======================================================