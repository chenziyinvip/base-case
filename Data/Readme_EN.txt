##Detailed information about the dataset:
URL: https://www.researchgate.net/publication/285577915_Representative_electrical_load_profiles_of_residential_buildings_in_Germany_with_a_temporal_resolution_of_one_second
DOI:  DOI: 10.13140/RG.2.1.3713.1606 

download link: 
https://pvspeicher.htw-berlin.de/veroeffentlichungen/daten/lastprofile/

##Zitation:
HTW Berlin � University of Applied Sciences: "Representative electrical load profiles of residential buildings in Germany with a temporal resolution of one second", dataset, Berlin, 2015.

##Content of the dataset
PL1					-> Matrix [31536000/525600x74] -> one year active power, phase 1 in W on a one second ["_1s_"] / one minute ["_1min_"] timescale, column represents No. of the load profile
PL2					-> Matrix [31536000/525600x74] -> one year active power, phase 2 in W on a one second ["_1s_"] / one minute ["_1min_"] timescale, column represents No. of the load profile
PL3					-> Matrix [31536000/525600x74] -> one year active power, phase 3 in W on a one second ["_1s_"] / one minute ["_1min_"] timescale, column represents No. of the load profile
QL1					-> Matrix [31536000/525600x74] -> one year reactive power, phase 1 in var on a one second ["_1s_"] / one minute ["_1min_"] timescale, column represents No. of the load profile
QL2					-> Matrix [31536000/525600x74] -> one year reactive power, phase 2 in var on a one second ["_1s_"] / one minute ["_1min_"] timescale, column represents No. of the load profile
QL3					-> Matrix [31536000/525600x74] -> one year reactive power, phase 3 in var on a one second ["_1s_"] / one minute ["_1min_"] timescale, column represents No. of the load profile
												(postive reactive power: inductive | negative reative power: capacitive)
time_datenum_MEZ 	-> Matrix [31536000/525600x1]  -> timestamp in local winter time (GMT+1) as matlab format http://de.mathworks.com/help/matlab/ref/datenum.html
time_datevec_MEZ	-> Matrix [31536000/525600x6]  -> timestamp in local winter time (GMT+1)
								column 1 = year
								column 2 = month of the year 	[1..12]
								column 3 = day of the year 		[1..31]
								column 4 = hour of the day 		[0..23]
								column 5 = minute of the hour 	[0..59]
								column 6 = second of the minute	[0..59]
##disk space
["_1s_"]
Matlab-File (*.mat) = 4.08 GB
CSV-File (*.csv) = 5.44 GB zipped and 45 GB unzipped
["_1min_"]
Matlab-File (*.mat) = 0.17 GB
CSV-File (*.csv) = 0.21 GB zipped and 0.7 GB unzipped

##Publications
Publications from our research group based on this Dataset can be found under:
http://pvspeicher.htw-berlin.de
