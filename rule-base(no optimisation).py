#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 16:04:57 2020

@author: chenziyin
"""
import numpy as np
import pandas as pd
import os

#----------------------------------------------------------------------------------
# Initializatin 
#----------------------------------------------------------------------------------

#df = pd.read_excel("/Users/chenziyin/Desktop/datasheet.xlsx",\
                   #header=None,sheet_name=0,skip_footer=0,usecols=[1],names=None)
# Load PV generation 
df = pd.read_excel(os.getcwd()+"\Data\datasheet.xlsx",skipfooter=0)
 # Load loads 

# define a size of the loaded data
 
# define data structures for storing results
c = np.zeros(simulation_time+1) #battery capacity  
p_b = np.zeros(simulation_time) # battery power
p_pcc = np.zeros(simulation_time)

# initial condition 
# initial valaues 
c[0] = 1


# Battery parameters 
p_bmax = 2.5 # in kW
p_pvmax = 4 # in kWp
cmax = 4.5  # Maximum SOC in kWh 
cmin = 1 # Minimum SOC in kWh


# Defining simulation data
delta_t = 0.25 # in hours 

# Is it necessary to do it? 
df_li = df.values.tolist()
result = []
for s_li in df_li:
    result.append(s_li[0])
p_load=result

# This part should be removed 
df = pd.read_excel(os.getcwd()+"\datasheet.xlsx",skipfooter=0)

df_li = df.values.tolist()
result = []
for s_li in df_li:
    result.append(s_li[0])
b = result
p_pv = []
a = np.zeros(96)
for i in range(0,288,3):
    p_pv.append((b[i]+b[i+1]+b[i+2])/3)


# Simulation 
for i in range(0, simulation_time):
    # run battery controler 
  
     p_pcc[i] = p_pv[i] - p_load[i] - p_b[i]
     # SOC estimation assuming that battery did what you wanted 
     c[i+1] = c[i] + p_b[i]*delta_t      
     

for i in range(0, 95):
    t = 0.25 * i
    print('T=: ', t)
    print('power exchange with grid is: ', p_pcc[i])
    if p_pv[i] > p_load[i]:
        print('battery charging power is: ', p_b[i])
    
    else:    
        print('battert discharging power is: ', p_b[i])

# write in some data format, file 
print('The sum of power exchange with grid is: ', sum(p_pcc))
        



