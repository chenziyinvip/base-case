#in this modification i added the power exchange between heat buffer and the room temperature. y_buffer2(0)...y_buffer2(96)
from gurobipy import *
import numpy as np
import pandas as pd

#input the parameters

limit = np.zeros(95)
tau = 96
p_bmax = 4 #kw
C_n = 10 #kwh
C_max = 4 #kwh
C_min = 1 #kwh
Delta_t = 0.25 #hour
V_buffer = 300 #L
V_DHW = 300 #L
p_roomloss = 0.7 #kw
p_DHWloss = 0.5 #kw
p_bufferloss = 0.5 #kw
water_density = 1 #kg/L
C_water = 0.001163 #kwh/kg*K
T_DHWmin = 45 #degree
T_buffermin = 32 #degree
coeff_building = 16 #kwh/K




#import the data from local file
df = pd.read_excel("/Users/chenziyin/Desktop/datasheet.xlsx",\
                   header=None,sheet_name=0,skip_footer=0,usecols=[1],names=None)
df_li = df.values.tolist()
result = []
for s_li in df_li:
    result.append(s_li[0])
p_load=result[0:96]  #LOAD consumption sequence/kw


df = pd.read_excel("/Users/chenziyin/Desktop/datasheet.xlsx",\
                   header=None,sheet_name=0,skip_footer=0,usecols=[0],names=None)
df_li = df.values.tolist()
result = []
for s_li in df_li:
    result.append(s_li[0])
b = result
p_pv = []
a = np.zeros(96)
for i in range(0,288,3):
    p_pv.append((b[i]+b[i+1]+b[i+2])/3)  #PV generation sequence/kw 
    
V_water1 = [0,0,0,0,0,0,0,600,120,0,0,0,240,0,0,0,0,0,240,120,262.5,0,0,0] #L
c=[]
for item in V_water1:
    b.append(item/4)
V_water = [val for val in b for i in range(4)] #L

m_water = []
for item in V_water:
    m_water.append(item * water_density) #kg



M_water_DHW = water_density * V_DHW #kg
M_water_buffer = water_density * V_buffer #kg

#set list a and b for the constraints index, a presents the time ponits when p_pv > p_load, b presents p_pv < p_load
def idx(p_pv, p_load):
    alist = []
    blist = []
    for t in range(0, tau-1):
        if p_pv[t]>=p_load[t]:
            alist.append(t)
        else:
           blist.append(t)
    return alist, blist

a, b = idx(p_pv, p_load)


#set the solution model MIP   
m = Model('mip')
 
#set variables matrix
# |x0(0),x0(1),x0(2).....x0(96)| soc of battery at each time point, continuous value
# |x1(0),x1(1),x1(2).....x1(96)| power of battery at each time poinrt, continuous value
# |y_DHW0(0),y_DHW0(1),y_DHW(2).....y_DHW0(96)| temp of DHW
# |y_DHW1(0),y_DHW1(1),y_DHW1(2).....y_DHW1(96)| charging power of DHW
# |y_buffer0(0),y_buffer0(1),y_buffer0(2).....y_buffer0(96)| temp of buffer
# |y_buffer1(0),y_buffer1(1),y_buffer1(2).....y_buffer1(96)| charging power of buffer
# |y_buffer2(0),y_buffer2(1),y_buffer2(2).....y_buffer2(96)| power exchange between heat buffer and room temperature
# |y_room0(0),y_room0(1),y_room0(2).....y_room0(96)| temp of room
# |y_room1(0),y_room1(1),y_room1(2).....y_room1(96)| charging power buffer
# |y_HP0(0),y_HP0(1),y_HP0(2).....y_HP0(96)| power of HP
# |Temp0(0)| T max of DHW
# |Temp1(0)| T max of buffer


x_battery = m.addVars(2, 96, vtype=GRB.CONTINUOUS, name='x')
y_DHW = m.addVars(2, 96, vtype=GRB.CONTINUOUS, name='y_DHW')
y_buffer = m.addVars(3, 96, vtype=GRB.CONTINUOUS, name='y_buffer')
y_room = m.addVars(2, 96, vtype=GRB.CONTINUOUS, name='y_room')
y_HP = m.addVars(1, 96, vtype=GRB.INTEGER, name='y_HP')

Temp = m.addVars(2,1, vtype=GRB.CONTINUOUS, name='Temp')

#set initial values 
x_battery[0, 0] = 1 #initial capacity of battery
#y_DHW[0, 0] = 47
#y_buffer[0, 0] = 33
#y_room[0, 0] = 33



#set constraints for variables
#'===================================================================================================='
#constraints for battery and heat storage transfer functions
m.addConstrs((x_battery[0, t] - x_battery[0, t+1] + x_battery[1, t] * Delta_t == 0 for t in a), 'C1')
m.addConstrs((x_battery[0, t] - x_battery[0, t+1] - x_battery[1, t] * Delta_t == 0 for t in b), 'C2')  

m.addConstrs((C_water * (M_water_DHW - m_water[t]) * y_DHW[0, t] + y_DHW[1, t] * Delta_t - p_DHWloss * Delta_t == C_water * M_water_DHW * y_DHW[0, t+1] for t in range(tau-1)), 'C3')
m.addConstrs((C_water * M_water_buffer * y_buffer[0, t] - p_bufferloss * Delta_t + y_buffer[1, t] * Delta_t - y_buffer[2, t] * Delta_t == C_water * M_water_buffer * y_buffer[0, t+1] for t in range(tau-1)), 'C4')
m.addConstrs((coeff_building * y_room[0, t] - p_roomloss * Delta_t + y_room[1, t] * Delta_t + y_buffer[2, t] * Delta_t == coeff_building  * y_buffer[0, t+1] for t in range(tau-1)), 'C5')
m.addConstrs((y_DHW[1, t] + y_buffer[1, t] + y_room[1, t] == y_HP[0, t] for t in range(tau-1)), 'C6')
#'====================================================================================================='
#constraints for battery soc
m.addConstrs((x_battery[0, t] <= C_max for t in range(tau)), 'C7') #max usable capacity of battery is 9
   
m.addConstrs((x_battery[0, t] >= C_min for t in range(tau)), 'C8') #min usable capacity of battery is 1
#'======================================================================================================'      
for t in range(tau-1):
    limit[t] = p_pv[t] - p_load[t]

#constraints for battery charging power
m.addConstrs((x_battery[1, t] >= 0 for t in a) , 'C9') #battert charging powei is positive
m.addConstrs((x_battery[1, t] <= p_bmax for t in a) , 'C10') #battery charging power is less than p_bmax
#m.addConstrs((x_battery[1, t] <= limit[t] for t in a) , 'C11')


#constraints for battery discharging power
m.addConstrs((x_battery[1, t] >= 0 for t in b), 'C12') #battert discharging powei is positive
m.addConstrs((x_battery[1, t] <= p_bmax for t in b), 'C13') #battery discharging power is less than p_bmax
#m.addConstrs((x_battery[1, t] <= -limit[t] for t in b), 'C14')

#'======================================================================================================'   
m.addConstrs((y_buffer[2, t] >= 0 for t in range(tau)), 'C22')
m.addConstrs((y_buffer[2, t] <= 5 for t in range(tau)), 'C23') # 5 is the max output power of heat buffer

m.addConstrs((y_DHW[0, t] >= T_DHWmin for t in range(tau)), 'C15')
m.addConstrs((y_DHW[0, t] <= Temp[0, 0] for t in range(tau)), 'C16')

m.addConstrs((y_buffer[0, t] >= T_buffermin for t in range(tau)), 'C17')
m.addConstrs((y_buffer[0, t] <= Temp[1, 0] for t in range(tau)), 'C18')

m.addConstrs((y_room[0, t] >= 20 for t in range(tau)), 'C19')
m.addConstrs((y_room[0, t] <= 35 for t in range(tau)), 'C20')


m.addConstrs((y_HP[0, t] <= 15 for t in range(tau)), 'C21')




#set the objective function 



def obj1(tau):
    S = 0
    for t in range(tau-1):
        if p_pv[t]>p_load[t]:
            if p_pv[t]-p_load[t]-x_battery[1, t] >= y_HP[0, t]:
                S = S + p_pv[t]-p_load[t]-x_battery[1, t]-y_HP[0, t]
            else:
                S = S - (p_pv[t]-p_load[t]-x_battery[1, t]-y_HP[0, t])
        else:
            S = S + (-p_pv[t]+p_load[t]-x_battery[1, t]+y_HP[0, t])
    return S

def obj2(tau):
    S = 0
    for t in range(tau-1):
        if p_pv[t]>p_load[t]:
            S = S + (p_pv[t]-p_load[t]-x_battery[1, t]-y_HP[0, t])*(p_pv[t]-p_load[t]-x_battery[1, t]-y_HP[0, t])
        else:
            S = S + (-p_pv[t]+p_load[t]+x_battery[1, t]+y_HP[0, t])*(-p_pv[t]+p_load[t]+x_battery[1, t]+y_HP[0, t])
    return S

#solve the problem
m.setObjective(obj2(tau), GRB.MINIMIZE)

m.optimize()

print('the T_max of buffer= ', Temp[1, 0])
print('the T_max of DHW = ', Temp[0, 0])
for t in range(tau-1):
    print('TIME=:', t)
    print('battery_soc:', x_battery[0, t])
    print('room temperature = ', y_room[0, t])
    print('the power of HP = ', y_HP[0, t])
    if t in a:
        print('battery_charging_power:', x_battery[1, t])
    elif t in b:
        print('battery_discharging_power:',x_battery[1, t])
    print('=======================================================')


    
    
    