#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  3 00:44:58 2020

@author: chenziyin
"""

from gurobipy import *
import numpy as np
import os 

#input the parameters
import pandas as pd


df = pd.read_excel(os.getcwd()+"\Data\datasheet.xlsx",skipfooter=0)
df_li = df.values.tolist()
result = []
for s_li in df_li:
    result.append(s_li[0])
p_load=result[0:96]  #LOAD consumption sequence/kw



df_li = df.values.tolist()
result = []
for s_li in df_li:
    result.append(s_li[0])
b = result
p_pv = []
a = np.zeros(96)
for i in range(0,288,3):
    p_pv.append((b[i]+b[i+1]+b[i+2])/3)  #PV generation sequence/kw 
    
tau = len(p_pv)  
limit = np.zeros(tau-1)
p_bmax = 4 #kw
C_n = 10 #kwh
C_max = 4 #kwh
C_min = 1 #kwh
Delta_t = 0.25 #hour

#p_pvmax = 10 #kw pro kwp

#set list a and b for the constraints index, a presents the time ponits when p_pv > p_load, b presents p_pv < p_load
def idx(p_pv, p_load):
    alist = []
    blist = []
    for t in range(0, tau-1):
        if p_pv[t]>=p_load[t]:
            alist.append(t)
        else:
            blist.append(t)
    return alist, blist

a, b = idx(p_pv, p_load)


#set the solution model MIP   
m = Model('mip')
 
#set variables matrix
# |x0(0),x0(1),x0(2).....x0(8)| soc of battery at each time point, continuous value

# |x1(0),x1(1),x1(2).....x1(8)| power of battery at each time poinrt, continuous value

x = m.addVars(2, 96, vtype=GRB.CONTINUOUS, name='x')

#set initial values 
x[0, 0] = 1 #initial capacity of battery



#set constraints for variables
#'===================================================================================================='
#constraints for battery
m.addConstrs((x[0, t] - x[0, t+1] + x[1, t] * Delta_t == 0 for t in a), 'C1')
m.addConstrs((x[0, t] - x[0, t+1] - x[1, t] * Delta_t == 0 for t in b), 'C2')  

#'====================================================================================================='
#constraints for battery soc
m.addConstrs((x[0, t] <= C_max for t in range(tau)), 'C3') #max usable capacity of battery 
   
m.addConstrs((x[0, t] >= C_min for t in range(tau)), 'C4') #min usable capacity of battery 
#'======================================================================================================'      
for t in range(tau-1):
    limit[t] = p_pv[t] - p_load[t]

#constraints for battery charging power
m.addConstrs((x[1, t] >= 0 for t in a) , 'C5') #battert charging powei is positive
m.addConstrs((x[1, t] <= p_bmax for t in a) , 'C6') #battery charging power is less than p_bmax
m.addConstrs((x[1, t] <= limit[t] for t in a) , 'C7') 


#constraints for battery discharging power
m.addConstrs((x[1, t] >= 0 for t in b), 'C8') #battert discharging powei is positive
m.addConstrs((x[1, t] <= p_bmax for t in b), 'C9') #battery discharging power is less than p_bmax
m.addConstrs((x[1, t] <= -limit[t] for t in b), 'C10')

 #'======================================================================================================'    

#set the objective function 

def obj(tau):
    S = 0
    for t in range(tau-1):
        if p_pv[t]>p_load[t]:
            S = S + (p_pv[t]-p_load[t]-x[1, t])
        else:
            S = S + (-p_pv[t]+p_load[t]-x[1, t])
    return S


#solve the problem
m.setObjective(obj(tau), GRB.MINIMIZE)

m.optimize()

for t in range(tau):
    print('TIME=:', t)
    print('battery_soc:', x[0, t])
    if t in a:
        print('battery_charging_power:', x[1, t])
    elif t in b:
        print('battery_discharging_power:',x[1, t])
    print('=======================================================')




