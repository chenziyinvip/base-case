# -*- coding: utf-8 -*-
"""
Created on Tue May 12 16:56:15 2020
Rule based controller of the battery 
@author: mbo
"""
# inputs 
# P_PV, P_load - battery controller operates  
# P_batt_max_ current SOC, SOC_min_SOC_max, delta_t - battery parameters   
if p_pv[i] > p_load[i]:
        p_b[i] = min(p_bmax, (cmax-c[i])/delta_t, p_pv[i]-p_load[i])
        #if p_pv[i] - p_load[i] - p_b[i] > 0.7 * p_pvmax:
            #p_cc[i] = 0.7 * p_pvmax
        #else:
        
          
    else:
        p_b[i] = min(p_bmax, (c[i]-cmin)/delta_t, p_load[i]-p_pv[i])
        p_pcc[i] = p_load[i] - p_pv[i] - p_b[i]
        c[i+1] = c[i] - p_b[i] * delta_t
# return P_battery 